package com.example.testchart

import android.graphics.Color
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import com.github.mikephil.charting.charts.LineChart
import com.github.mikephil.charting.components.YAxis.AxisDependency
import com.github.mikephil.charting.data.Entry
import com.github.mikephil.charting.data.LineData
import com.github.mikephil.charting.data.LineDataSet

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        setupChart()
    }

    private fun setupChart() {
        // генерим данные, можно свои подставить
        val dataObjects = MyData.generateDoubleData()

        // лист entry для графика
        val entries = getEntriesFromData(dataObjects)

        // добавим entries to набор dataset
        val dataSet = getDataSetFromEntries(entries)

        // создаем пустой график, но со свойствами
        val chart: LineChart = getChart(findViewById(R.id.chart))

        // строим точки на графике
        val lineData: LineData = LineData(dataSet)

        chart.setData(lineData)

        // отрисовать график в окне
        chart.invalidate()
    }

    private fun <T> getEntriesFromData(dataObjects: List<MyData<T>>): List<Entry> {
        val entries: MutableList<Entry> = mutableListOf()

        try {
            for (data in dataObjects) {

                // entries принимает токма Float, конвертим
                val x = data.x.toString().toFloatOrNull() ?: 0f
                val y = data.y.toString().toFloatOrNull() ?: 0f

                // turn your data into Entry objects
                entries.add(Entry(x, y))
            }
        } catch (e: Exception) {
            Log.e("myTag", " ошибка ${e.message}")
        } finally {
            return entries
        }
    }

    private fun getDataSetFromEntries(entries: List<Entry>): LineDataSet {
        val dataSet: LineDataSet = LineDataSet(entries, "Label");

        // цвета и толщина линий
        dataSet.setColor(Color.GREEN);
        dataSet.setLineWidth(3f)
        dataSet.setCircleRadius(1f)
        dataSet.setCircleColor(Color.GREEN)
        dataSet.disableDashedLine()

        return dataSet
    }

    private fun getChart(chart: LineChart): LineChart {
        // отключить сетку координат
        chart.xAxis.isEnabled = false
        chart.axisLeft.isEnabled = false
        chart.axisRight.isEnabled = false

        chart.setDrawGridBackground(false)
        chart.setDrawBorders(false)
        chart.setDrawMarkers(false)

        // фон для графика
        chart.setBackgroundColor(Color.BLACK)

        return chart
    }
}