package com.example.testchart

data class MyData<T>(val x: T, val y: T) {
    companion object {
        /**
         * задать int список координат
         */
        fun generateIntData(): List<MyData<Int>> {
            return listOf(
                MyData(x = 1, y = 4),
                MyData(x = 3, y = 5),
                MyData(x = 4, y = 8),
                MyData(x = 5, y = 9),
                MyData(x = 5, y = 6),
                MyData(x = 6, y = 5),
                MyData(x = 7, y = 4),
                MyData(x = 8, y = 4),
                MyData(x = 8, y = 5),
                MyData(x = 9, y = 7),
                MyData(x = 11, y = 8),
                MyData(x = 12, y = 1),
                MyData(x = 14, y = 2),
                MyData(x = 15, y = 4),
                MyData(x = 17, y = 5),
                MyData(x = 19, y = 4),
                MyData(x = 20, y = 7),
                MyData(x = 23, y = 11),
                MyData(x = 25, y = 12)
            )
        }

        /**
         * задать double список координат
         */
        fun generateDoubleData(): List<MyData<Double>> {
            return listOf(
                MyData(x = 1.0, y = 4.0),
                MyData(x = 3.0, y = 5.0),
                MyData(x = 4.0, y = 8.0),
                MyData(x = 5.0, y = 9.0),
                MyData(x = 5.0, y = 6.0),
                MyData(x = 6.0, y = 5.0),
                MyData(x = 7.0, y = 4.0),
                MyData(x = 8.0, y = 4.0),
                MyData(x = 8.0, y = 5.0),
                MyData(x = 9.0, y = 7.0),
                MyData(x = 11.0, y = 8.0),
                MyData(x = 12.0, y = 1.0),
                MyData(x = 14.0, y = 2.0),
                MyData(x = 15.0, y = 4.0),
                MyData(x = 17.0, y = 5.0),
                MyData(x = 19.0, y = 4.0),
                MyData(x = 20.0, y = 7.0),
                MyData(x = 23.0, y = 11.0),
                MyData(x = 25.0, y = 12.0)
            )
        }
    }


}
